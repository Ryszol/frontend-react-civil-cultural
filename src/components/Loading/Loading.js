import React, { Fragment } from 'react'

import './loading.css'

export default function Loading(props) {
  return (
    <Fragment>
      <div className="spin-wrapper">
        <div className={"spinner spinner" + props.size}></div>
      </div>
    </Fragment>
  )
}