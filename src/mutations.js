import gql from 'graphql-tag';

export const DELETE_NEWS = gql`
  mutation deleteNews($id: ID!) {
    deleteNews(input: {
      id: $id
    }) {
      news {
        id
        title
        body
      }
    }
  }
`

export const CREATE_NEWS = gql`
  mutation createNews($title: String!, $body: String!, $portal: ID!) {
    ceateNews(input: {
      title: $title
      body: $body
      portal: $portal
    }) {
      news {
        id
        title
        body
      }
    }
  }
`;

export const CREATE_PORTAL = gql`
  mutation createPortal($name: String!) {
    createPortal(input: {
      name: $name
      isPublic: true
    }) {
      portal {
        id
        name
      }
    }
  }
`;


export const LOGIN_MUTATION = gql`
  mutation login($username: String!, $password: String!) {
    logIn(username: $username, password: $password) {
      token
    }
  }
`;

export const REGISTER_MUTATION = gql`
  mutation register($username: String!, $email: String!, $password: String!, $birthdate: Date!) {
    signUp(input:{
      username: $username,
      password: $password,
      email: $email,
      birthDate: $birthdate,
      clientMutationId: $username
    }) {
      clientMutationId
    }
  }
`;

export const LOGOUT = gql`
  mutation logout {
    logOut(input:{}) {
      response
    }
  }
`;